// function selector
const game = () => {
    const rockOpt = document.querySelector('.rockx');
    const paperOpt = document.querySelector('.paperx');
    const scissorOpt = document.querySelector('.scissorsx');
    // const rockCOpt = document.querySelector('.rocky');
    // const paperCOpt = document.querySelector('.papery');
    // const scissorCOpt = document.querySelector('.scissorsy');
    const playerOptions = [rockOpt, paperOpt, scissorOpt];
    const comOptions = ['rock', 'paper', 'scissor'];

    // function to start the game
    playerOptions.forEach(option => {
        option.addEventListener ('click', function(){

            const randomNumber = Math.floor(Math.random()*3);
            const comChoice = comOptions[randomNumber];

            // function to check the winner
            winner(this.innerText, comChoice);

        })
    })
}

// function to decide the winner
const winner = (player, com) => {
    const result = document.querySelector('.result');

    if (player === com){
        result.textContent = 'DRAW'
    }
    else if (player == 'rock'){
        if (com == 'scissor'){
            result.textContent = 'PLAYER 1 WIN';

        }else{
            result.textContent = 'COM WIN';

        }
    }
    else if (player == 'paper'){
        if (com == 'rock'){
            result.textContent = 'PLAYER 1 WIN';

        }else{
            result.textContent = 'COM WIN';

        }
    }
    else if (player == 'scissor'){
        if (com == 'paper'){
            result.textContent = 'PLAYER 1 WIN';

        }else{
            result.textContent = 'COM WIN';

        }
    }
}

// calling game function
game();